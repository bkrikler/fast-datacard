=======
History
=======

0.1.3 (2018-04-05)
------------------

* Easier handling of dataframe files

0.1.2 (2018-04-04)
------------------

* Updated executable name and documentation

0.1.1 (2018-10-01)
------------------

* added initial documentation

0.1.0 (2018-08-21)
------------------

* First release on PyPI.
