imax    *     number of categories
jmax    *     number of samples minus one
kmax    *     number of nuisance parameters
-------------------------------------------------------------------------------
shapes * * eq1b_eq5j_1200_inf_shapes.root $CHANNEL/$PROCESS $CHANNEL/$PROCESS_$SYSTEMATIC

bin	 Signal	DoubleMu	SingleMu	
observation	167	83	93	
-------------------------------------------------------------------------------
bin	Signal	Signal	Signal	Signal	DoubleMu	DoubleMu	SingleMu	SingleMu	

process	 VBF	Zinv	Ttw	Qcd	VBF	Ewk	VBF	Ewk	
process	 0	1	2	3	0	1	0	1	
rate	 1e-12	56.391628772	90.9463481903	51.5099272728	1e-12	30.5824260712	1e-12	23.2715797424	
-------------------------------------------------------------------------------
ElectronSFWeight shape	1	-	-	-	1	-	1	-	
bosonPtEwkWeight shape	1	1	1	1	1	1	1	1	
bosonPtQcdWeight shape	1	1	1	1	1	1	1	1	
bosonPtWeight shape	1	1	1	1	1	1	1	1	
bsfCFbWeight shape	1	-	-	-	1	-	1	-	
bsfCFcWeight shape	1	-	-	-	1	-	1	-	
bsfCFlWeight shape	1	-	-	-	1	-	1	-	
bsfLightWeight shape	1	1	1	1	1	1	1	1	
bsfWeight shape	1	1	1	1	1	1	1	1	
jecWeight shape	-	1	1	1	-	1	-	1	
lumiSyst lnN	1.026	1.06	-	-	1.026	1.06	1.026	1.06	
muonSfWeight shape	1	1	1	1	1	1	1	1	
muonTrackWeight shape	1	-	-	-	1	-	1	-	
nIsrWeight shape	1	1	1	1	1	1	1	1	
photonSFWeight shape	1	1	1	1	1	1	1	1	
photonTriggerWeight shape	1	1	1	1	1	1	1	1	
puWeight shape	1	1	1	1	1	1	1	1	
triggerWeight shape	1	1	1	1	1	1	1	1	
xsWeightTt shape	1	1	1	1	1	1	1	1	
xsWeightW shape	1	1	1	1	1	1	1	1	
