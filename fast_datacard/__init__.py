# -*- coding: utf-8 -*-

"""Top-level package for fast-datacard."""

__author__ = """F.A.S.T"""
__email__ = 'fast-hep@cern.ch'
__version__ = '0.1.3'
