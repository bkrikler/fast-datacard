#!/usr/bin/env bash

DST=$PWD/external/miniconda
if [ ! -d $DST ]
then
  wget https://repo.continuum.io/miniconda/Miniconda2-latest-MacOSX-x86_64.sh
  bash Miniconda2-latest-MacOSX-x86_64.sh -b -p $DST
  rm -f Miniconda2-latest-MacOSX-x86_64.sh
  export PATH=$DST/bin:$PATH
  conda create -n fast python=2.7 -y
  conda config --add channels http://conda.anaconda.org/NLeSC
  conda config --set show_channel_urls yes
  source activate fast
  conda install -y -q \
    numpy \
    root>=6.04 \
    rootpy
  source deactivate fast
else
  export PATH=$DST/bin:$PATH
fi
source activate fast
